#include "biscio.h"

#include <SDL.h>

/* function declarations */
static struct input *transinput(const SDL_Keycode k, const Uint8 state);

/* variables */
static SDL_Renderer *renderer;
static SDL_Window   *sdlwin;

/* function implementations */
void
draw_background(void)
{
	struct rect r;
	struct color c;

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(renderer, NULL);

	/* border */
	r.x = 0;
	r.y = 0;
	r.w = RES_WIDTH;
	r.h = 1;
	c.r = 255;
	c.g = 255;
	c.b = 255;
	c.a = 255;
	draw_rect(&r, &c);
	r.w = 1;
	r.h = RES_HEIGHT;
	draw_rect(&r, &c);
	r.x = RES_WIDTH - 1;
	draw_rect(&r, &c);
	r.x = 0;
	r.y = RES_HEIGHT - 1;
	r.w = RES_WIDTH;
	r.h = 1;
	draw_rect(&r, &c);
}

void
draw_rect(const struct rect *r, const struct color *c)
{
	SDL_Rect sr;

	sr.x = r->x;
	sr.y = r->y;
	sr.w = r->w;
	sr.h = r->h;

	SDL_SetRenderDrawColor(renderer, c->r, c->g, c->b, c->a);
	SDL_RenderFillRect(renderer, &sr);
}

void
handle_events(void)
{
	SDL_Event e;

	while (SDL_PollEvent(&e)) {
		switch (e.type) {
		case SDL_KEYDOWN:
			handle_input(transinput(e.key.keysym.sym, e.key.state));
			break;
		case SDL_QUIT:
			running = 0;
			break;
		case SDL_WINDOWEVENT:
			if (e.window.event == SDL_WINDOWEVENT_RESIZED) {

			}
			break;
		}
	}
}

void
init(void)
{
	SDL_Init(SDL_INIT_VIDEO);
	sdlwin = SDL_CreateWindow(
		"biscio",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		RES_WIDTH,
		RES_HEIGHT,
		SDL_WINDOW_RESIZABLE
	);
	renderer = SDL_CreateRenderer(sdlwin, -1, SDL_RENDERER_SOFTWARE);
	SDL_RenderSetLogicalSize(renderer, RES_WIDTH, RES_HEIGHT);
}

void
quit(void)
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(sdlwin);
	SDL_Quit();
}

void
sleep(int ms)
{
	SDL_Delay(ms);
}

struct input *
transinput(const SDL_Keycode k, const Uint8 state)
{
	static struct input i;

	i.state = state == SDL_PRESSED ? INPUT_PRESSED : INPUT_RELEASED;

	if (pause && k != SDLK_p && k != SDLK_q) {
		i.type = IGNORE_INPUT;
		return &i;
	}

	switch (k) {
	case SDLK_DOWN:
		i.type = INPUT_DOWN;
		break;
	case SDLK_ESCAPE:
	case SDLK_q:
		i.type = INPUT_ESC;
		break;
	case SDLK_LEFT:
		i.type = INPUT_LEFT;
		break;
	case SDLK_p:
		i.type = INPUT_PAUSE;
		break;
	case SDLK_RIGHT:
		i.type = INPUT_RIGHT;
		break;
	case SDLK_UP:
		i.type = INPUT_UP;
		break;
	default:
		i.type = IGNORE_INPUT;
	}

	return &i;
}

void
update_screen(void)
{
	SDL_RenderPresent(renderer);
}
