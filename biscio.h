/* macros */
#define IGNORE_INPUT -1
#define INPUT_DOWN  0
#define INPUT_ESC   1
#define INPUT_LEFT  2
#define INPUT_PAUSE 3
#define INPUT_RIGHT 4
#define INPUT_UP    5
#define INPUT_PRESSED  1
#define INPUT_RELEASED 0
#define RES_HEIGHT 180
#define RES_WIDTH 320

struct color {
	unsigned int r;
	unsigned int g;
	unsigned int b;
	unsigned int a;
};

struct input {
	int type;
	int state;
};

struct rect {
	unsigned int h;
	unsigned int w;
	int x;
	int y;
};

/* function declarations */
void draw_background(void);
void draw_rect(const struct rect *r, const struct color *c);
void handle_events(void);
void handle_input(const struct input *i);
void init(void);
void quit(void);
void sleep(int ms);
void update_screen(void);

/* variables */
extern int pause;
extern int running;
