#include "biscio.h"

#include <stdio.h>
#include <stdlib.h>

/* macros */
#define MAX_SNAKE_PARTS 100

enum cardinal {
	north = 0,
	east  = 1,
	south = 2,
	west  = 3
};

struct point {
	int x;
	int y;
};

struct snake_part {
	enum cardinal direction;
	int in_use;
	int length;
	struct snake_part *next;
	struct point origin;
	struct snake_part *previous;
};

struct snake {
	struct snake_part *ass;
	struct color color;
	struct snake_part *head;
	unsigned int length;
	unsigned int speed;
	struct snake_part parts[MAX_SNAKE_PARTS];
	unsigned int width;
};

/* function declarations */
static int add_snake_part(struct snake *s, const struct point *o, const enum cardinal d);
static void draw_snake(struct snake *s);
static void move_snake(struct snake *s);
static void render(void);
static void turn_snake(struct snake *s, const enum cardinal d);
static void update_world(void);

/* variables */
static struct snake biscio;
int pause = 0;
int running = 1;

/* function implementations */
int
add_snake_part(struct snake *s, const struct point *o, const enum cardinal d)
{
	struct snake_part *p;
	unsigned int i;

	for (i = 0; i < MAX_SNAKE_PARTS; i++) {
		p = s->parts + i;
		if (p->in_use)
			p = NULL;
		else
			break;
	}
	if (p == NULL)
		return 0;

	p->direction = d;
	p->in_use = 1;
	p->length = 0;
	p->next = NULL;
	p->origin.x = o->x;
	p->origin.y = o->y;
	p->previous = s->head;

	s->head->next = p;
	s->head = p;

	return 1;
}

void
draw_snake(struct snake *s)
{
	struct color c;
	struct snake_part *p;
	struct rect r;

	p = s->ass;

	do {
		if (p->length != 0) {
			r.x = p->origin.x;
			r.y = p->origin.y;
			switch(p->direction) {
				case north:
				case south:
					r.h = p->length;
					r.w = s->width;
					break;
				case east:
				case west:
					r.h = s->width;
					r.w = p->length;
					break;
			}
			draw_rect(&r, &s->color);
		}
		p = p->next;
	} while (p != NULL);

	/* draw ass with different color */
	r.w = s->width;
	r.h = s->width;

	c.r = 255;
	c.g = 255;
	c.b = 0;
	c.a = 255;

	switch(s->ass->direction) {
		case north:
			r.x = s->ass->origin.x;
			r.y = s->ass->origin.y + s->ass->length - s->width;
			break;
		case east:
		case south:
			r.x = s->ass->origin.x;
			r.y = s->ass->origin.y;
			break;
		case west:
			r.x = s->ass->origin.x + s->ass->length - s->width;
			r.y = s->ass->origin.y;
			break;

	}

	draw_rect(&r, &c);

	/* draw head with different color */
	c.r = 255;
	c.g = 0;
	c.b = 255;
	c.a = 255;

	switch(s->head->direction) {
		case north:
		case west:
			r.x = s->head->origin.x;
			r.y = s->head->origin.y;
			break;
		case east:
			r.x = s->head->origin.x + s->head->length - s->width;
			r.y = s->head->origin.y;
			break;
		case south:
			r.x = s->head->origin.x;
			r.y = s->head->origin.y + s->head->length - s->width;
			break;
	}

	draw_rect(&r, &c);
}

void
move_snake(struct snake *s)
{
	if (s->ass == s->head) {
		switch(s->head->direction) {
			case north:
				s->head->origin.y--;
				break;
			case east:
				s->head->origin.x++;
				break;
			case south:
				s->head->origin.y++;
				break;
			case west:
				s->head->origin.x--;
				break;
		}
		return;
	}

	if (s->ass->length == 0) {
		s->ass->in_use = 0;
		s->ass = s->ass->next;
		s->ass->previous = NULL;
	}

	if (s->ass->length <= s->width)
		s->ass->direction = s->ass->next->direction;

	s->ass->length--;
	switch(s->ass->direction) {
		case east:
			s->ass->origin.x++;
			break;
		case south:
			s->ass->origin.y++;
			break;
	}

	s->head->length++;
	switch(s->head->direction) {
		case north:
			s->head->origin.y--;
			break;
		case west:
			s->head->origin.x--;
			break;
	}
}

void
handle_input(const struct input *i) {
	if (i->type == IGNORE_INPUT)
		return;

	switch(i->type) {
		case INPUT_DOWN:
			if (biscio.head->direction == north || biscio.head->direction == south)
				return;
			turn_snake(&biscio, south);
			break;
		case INPUT_ESC:
			running = 0;
			break;
		case INPUT_LEFT:
			if (biscio.head->direction == east || biscio.head->direction == west)
				return;
			turn_snake(&biscio, west);
			break;
		case INPUT_PAUSE:
			pause = !pause;
			break;
		case INPUT_RIGHT:
			if (biscio.head->direction == east || biscio.head->direction == west)
				return;
			turn_snake(&biscio, east);
			break;
		case INPUT_UP:
			if (biscio.head->direction == north || biscio.head->direction == south)
				return;
			turn_snake(&biscio, north);
			break;
	}
}

int
main(int argc, char *argv[])
{
	unsigned int i;

	init();

	biscio.color.r = 255;
	biscio.color.g = 255;
	biscio.color.b = 255;
	biscio.color.a = 255;

	biscio.length = 50;
	biscio.width = 2;

	for (i = 0; i < MAX_SNAKE_PARTS; i++)
		biscio.parts[i].in_use = 0;

	biscio.parts[0].direction = east;
	biscio.parts[0].in_use = 1;
	biscio.parts[0].length = biscio.length;
	biscio.parts[0].next = NULL;
	biscio.parts[0].origin.x = 50;
	biscio.parts[0].origin.y = 80;
	biscio.parts[0].previous = NULL;

	biscio.ass = biscio.parts;
	biscio.head = biscio.parts;

	while (running)	{
		handle_events();
		if (!pause)
			update_world();
		render();
		sleep(16);
	}

	quit();

	return 0;
}

void
render(void)
{
	draw_background();
	draw_snake(&biscio);
	update_screen();
}

void
turn_snake(struct snake *s, const enum cardinal d)
{
	int done;
	struct point o;

	done = 0;

	switch(d) {
		case north:
			o.y = s->head->origin.y;
			if (s->head->direction == east) {
				o.x = s->head->origin.x + s->head->length - s->width;
				done++;
			} else if (s->head->direction == west) {
				o.x = s->head->origin.x;
				done++;
			}
			break;
		case east:
			o.x = s->head->origin.x + s->width;
			if (s->head->direction == north) {
				o.y = s->head->origin.y;
				done++;
			} else if (s->head->direction == south) {
				o.y = s->head->origin.y + s->head->length - s->width;
				done++;
			}
			break;
		case south:
			o.y = s->head->origin.y + s->width;
			if (s->head->direction == east) {
				o.x = s->head->origin.x + s->head->length - s->width;
				done++;
			} else if (s->head->direction == west) {
				o.x = s->head->origin.x;
				done++;
			}
			break;
		case west:
			o.x = s->head->origin.x;
			if (s->head->direction == north) {
				o.y = s->head->origin.y;
				done++;
			} else if (s->head->direction == south) {
				o.y = s->head->origin.y + s->head->length - s->width;
				done++;
			}
			break;
	}

	if (!done)
		return;

	add_snake_part(s, &o, d);
}

void
update_world(void)
{
	move_snake(&biscio);
	/* TODO: handle collisions */
}
