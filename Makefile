BIN     = biscio
VERSION = 1.0

PREFIX = /usr/local

INCS = -I/usr/include/SDL2 -D_REENTRANT
LIBS = -lSDL2

CPPFLAGS_BISCIO = -DVERSION=\"${VERSION}\" ${CPPFLAGS}
OPTIMIZATION    = -Os
CFLAGS_BISCIO   = ${OPTIMIZATION} -ansi -pedantic ${INCS} ${CPPFLAGS_BISCIO} ${CFLAGS}
LDFLAGS_BISCIO  = ${LIBS} ${LDFLAGS}

SRC = biscio.c sdl.c
OBJ = ${SRC:.c=.o}

all: ${BIN}

.c.o:
	${CC} ${CFLAGS_BISCIO} -c $< -o $@
	
${BIN}: ${OBJ}
	${CC} -o ${BIN} ${OBJ} ${LDFLAGS_BISCIO}

clean:
	rm -f ${BIN} ${BIN}-${VERSION}.tar.gz *.o

debug:
	@make OPTIMIZATION=-ggdb

dist: clean
	mkdir ${BIN}-${VERSION}
	cp -R ${SRC} LICENSE Makefile README ${BIN}-${VERSION}
	tar -cf ${BIN}-${VERSION}.tar ${BIN}-${VERSION}
	gzip ${BIN}-${VERSION}.tar
	rm -fr ${BIN}-${VERSION}

install: ${BIN}
	# bin
	mkdir -p ${DESTDIR}${PREFIX}/games
	cp -f ${BIN} ${DESTDIR}${PREFIX}/games
	chmod 755 ${DESTDIR}${PREFIX}/games/${BIN}
	# doc
	mkdir -p ${DESTDIR}${PREFIX}/share/doc/${BIN}
	cp -f README ${DESTDIR}${PREFIX}/share/doc/${BIN}
	chmod 644 ${DESTDIR}${PREFIX}/share/doc/${BIN}/README

uninstall:
	rm -f ${DESTDIR}${PREFIX}/games/${BIN}
	rm -fr ${DESTDIR}${PREFIX}/share/doc/${BIN}

.PHONY: all options clean dist install uninstall
